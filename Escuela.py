"""
-- script para crear tablas en bd
-- la bd debe llamarse  escuela.db
-- tabla Alumno
CREATE TABLE "alumno" (
	"alu_id"	INTEGER NOT NULL,
	"alu_nombres"	TEXT NOT NULL,
	"alu_apellidos"	TEXT NOT NULL,
	"cur_id"	INTEGER,
	PRIMARY KEY("alu_id" AUTOINCREMENT)
)
-- tabla Curso
CREATE TABLE "curso" (
	"cur_id"	INTEGER NOT NULL,
	"cur_nombre"	TEXT NOT NULL,
	PRIMARY KEY("cur_id")
)

-- Tabla profesor
CREATE TABLE "profesor" (
	"pro_id"	INTEGER NOT NULL,
	"pro_nombre"	TEXT NOT NULL,
	"pro_apellido"	TEXT NOT NULL,
	"pro_catedra"	TEXT NOT NULL,
	PRIMARY KEY("pro_id" AUTOINCREMENT)
)

-- tabla Horario
CREATE TABLE "horario" (
	"hor_id"	INTEGER NOT NULL,
	"hor_dia"	TEXT NOT NULL,
	"hor_desde"	TEXT NOT NULL,
	"hor_hasta"	TEXT NOT NULL,
	"cur_id"	INTEGER NOT NULL,
	"pro_id"	INTEGER NOT NULL,
	PRIMARY KEY("hor_id" AUTOINCREMENT)
)

Nota validaciones de ingresos de datos del programa no realizadas.

"""

# -*- coding: utf-8 -*-
import sqlite3
import os.path
import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DB_PATH = os.path.join(BASE_DIR, "escuela.db")

class AlumnoNoRegistrado(Exception):
    pass

# CONEXION
class ConexionManager(object):
    def __init__(self, database=None):
        if not database:
            database = ':memory:'
        self.conn = sqlite3.connect(database)
        self.cursor = self.conn.cursor()

# ADMINISTRADORES

class AlumnoManager(ConexionManager):

    def guardarAlumno(self, obj):
        if obj.id != 0:
            query = 'UPDATE alumno SET alu_nombres="{}", alu_apellidos="{}", cur_id="{}" WHERE alu_id="{}"'.format(obj.nombres, obj.apellidos, obj.curso, obj.id)
        else:
            query = 'INSERT INTO alumno ("alu_nombres", "alu_apellidos","cur_id") VALUES  ("{}", "{}","{}")'.format(obj.nombres, obj.apellidos, obj.curso)  
    
        self.cursor.execute(query)
        self.conn.commit()
    
    def getAlumnoId(self,id):
        query = 'SELECT * FROM alumno WHERE alu_id="{}"'.format(id)
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        alumno = Alumno(result[0],result[1],result[2],result[3])
        return alumno

    def getAlumno(self, curso_id):
        query = 'SELECT * FROM alumno WHERE cur_id="{}"'.format(curso_id)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        if len(result)==0:
            print("No existen alumnos asociados al curso")
        elif curso_id != 0:
            print(f"Existen {len(result)} alumno(s) asociado(s) al curso")
        else:
            print(f"Existen {len(result)} alumno(s) sin curso")
        alumnos = []
        for data in result:
            alumno = Alumno(id=data[0], nombres=data[1], apellidos=data[2], curso=data[3])
            alumnos.append(alumno)
        return alumnos

class CursoManager(ConexionManager):
    
    def insertCurso(self, obj): 
        query = 'INSERT INTO curso ("cur_nombre") VALUES  ("{}")'.format(obj.nombre)
        self.cursor.execute(query)
        self.conn.commit() 

    def getCurso(self):
        query = 'SELECT * FROM curso ' 
        self.cursor.execute(query)
        result = self.cursor.fetchall()

        cursos = []
        for data in result:
            curso = Curso(id=data[0], nombre=data[1])
            cursos.append(curso)
        return cursos   

class ProfesorManager(ConexionManager):

    def guardarProfesor(self, obj):
        if obj.id != 0:
            query = 'UPDATE profesor SET pro_nombre="{}", pro_apellido="{}", pro_catedra="{}" WHERE pro_id="{}"'.format(obj.nombre, obj.apellido, obj.catedra, obj.id)
        else:
            query = 'INSERT INTO profesor ("pro_nombre", "pro_apellido", "pro_catedra") VALUES  ("{}", "{}","{}")'.format(obj.nombre, obj.apellido, obj.catedra)
    
        self.cursor.execute(query)
        self.conn.commit()
       
    def getProfesor(self):
        query = 'SELECT * FROM profesor ' 
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        profesores = []
        for data in result:
            profesor = Profesor(id=data[0], nombre=data[1], apellido=data[2], catedra=data[3])
            profesores.append(profesor)
        return profesores  

class HorarioManager(ConexionManager):

    def guardarHorario(self, obj):
        if obj.id != 0:
            query = 'UPDATE horario SET hor_dia="{}", hor_desde="{}", hor_hasta="{}", cur_id="{}", pro_id="{}" WHERE pro_id="{}"'.format(obj.dia, obj.desde, obj.hasta, obj.curso, obj.profesor, obj.id)
        else:
            query = 'INSERT INTO horario ("hor_dia", "hor_desde", "hor_hasta", "cur_id", pro_id) VALUES  ("{}", "{}", "{}", "{}", "{}")'.format(obj.dia, obj.desde, obj.hasta, obj.curso, obj.profesor)
        self.cursor.execute(query)
        self.conn.commit()
    
    def horarioProfesor(self,id_pro):
        query = '''SELECT p.pro_nombre, p.pro_apellido, c.cur_nombre, h.hor_dia, h.hor_desde, h.hor_hasta 
                   FROM profesor p 
                   INNER JOIN horario h ON p.pro_id=h.pro_id
                   INNER JOIN curso c ON c.cur_id=h.cur_id
                   WHERE p.pro_id={}'''.format(id_pro) 
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        horarios = []
        for data in result:
            horario = data[0]+" "+data[1], data[2], data[3], data[4], data[5]
            horarios.append(horario)
        return horarios  
    
    def horarioCurso(self,curso_id):
        query = '''SELECT c.cur_nombre, h.hor_dia, h.hor_desde, h.hor_hasta, p.pro_nombre, p.pro_apellido 
                   FROM curso c
                   INNER JOIN horario h ON c.cur_id=h.cur_id
                   INNER JOIN profesor p ON p.pro_id=h.pro_id
                   WHERE c.cur_id={}'''.format(curso_id) 
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        horarios = []
        for data in result:
            horario = data[0], data[1], data[2], data[3], data[4]+" "+data[5]
            horarios.append(horario)
        return horarios  

# MODELOS
class Alumno(object):
    "Alumno Model"
    objects = AlumnoManager(DB_PATH)

    def __init__(self, id, nombres, apellidos, curso):
        self.id = id
        self.nombres = nombres
        self.apellidos = apellidos
        self.curso = curso
    
    def __repr__(self):
        return u'{}.- {} {}'.format(self.id, self.nombres, self.apellidos)

class Profesor(object):
    "Profesor Model"
    objects = ProfesorManager(DB_PATH)

    def __init__(self, id, nombre, apellido, catedra):
        self.id = id
        self.nombre = nombre
        self.apellido = apellido
        self.catedra = catedra
    
    def __repr__(self):
        return u'{}.- prof. {} {} - {}'.format(self.id, self.nombre, self.apellido, self.catedra)


class Curso(object):
    "Curso Model"
    objects = CursoManager(DB_PATH)

    def __init__(self, id, nombre):
        self.id = id
        self.nombre = nombre
    
    def __repr__(self):
        return u'{}.- {}'.format(self.id, self.nombre)

class Horario(object):
    "Horario Model"
    objects = HorarioManager(DB_PATH)

    def __init__(self, id, dia, desde, hasta, curso, profesor):
        self.id = id
        self.dia = dia
        self.desde = desde
        self.hasta = hasta
        self.curso = curso
        self.profesor = profesor
    
    def __repr__(self):
        return u'{} {} {}'.format(self.dia, self.desde, self.hasta)



class EscuelaManager(object):

    def __init__(self):
        self.menu()

    def menu(self):
        while True:
            print(""" 
--------------------
------- Menú -------
--------------------
1. Crear cursos
2. Agregar Alumnos Nuevos
3. Agregar Profesores
4. Agregar Alumnos a Curso
5. Asignar horario y profesor a curso
6. Ver alumnos por curso
7. Ver horario del profesor
8. Ver Horario por curso

0. Salir
            """)
           
            opc = int(input("Ingrese opción ==> "))
            if opc==1:
                self.crearCurso()
            if opc==2:
                self.ingresarAlumno()
            if opc==3:
                self.agregarProfesor()    
            if opc==4:
                self.agregarAlumnoCurso()
            if opc==5:
                self.AsignarHorarioProfesorACurso()
            if opc==6:
                self.verAlumnosPorCurso()
            if opc==7:
                self.verHorarioProfesor() 
            if opc==8:
                self.verHorarioCurso()         
            if opc==0:
                self.salir()
        

    def crearCurso(self):
        print("""
        -----------------
        Crear de curso
        -----------------
        """)
        print()

        nivel_curso = int(input("Nivel [1:Básico | 2:Medio] "))
        
        if nivel_curso == 1: 
            nivel ="Básico"
            grados = "Básico: 1-2-3-4-5-6-7-8"
        else: 
            nivel='Medio'
            grados = "Medio: 1-2-3-4"
        grado_curso = int(input("Grado ["+ grados +"] "))
        seccion_curso = input("Seccion [A-B-C-D-F-G-H] ")
        nombre_curso = str(grado_curso) + " " + seccion_curso + " " + nivel
       
        Cur = Curso(id=0, nombre=nombre_curso)
        Curso.objects.insertCurso(Cur)
        print("Nuevo curso creado:", nombre_curso)
        print()  
    
    def ingresarAlumno(self):
        print("""
        -----------------
        Ingreso de Alumno
        -----------------
        """)
        print()
        nombres_alum = input("Nombres: ")
        apellido_alum = input("Apellidos: ")
        print()
        print("cursos disponibles")
        self._listar_cursos()
        curso_id = input("Elija Curso [0 para omitir]")
        Alum = Alumno(id=0, nombres=nombres_alum, apellidos=apellido_alum, curso=curso_id)
        Alumno.objects.guardarAlumno(Alum)
        print("Alumno Ingresado con éxito")
        self.pausa()

    def agregarProfesor(self):
        print("""
        -----------------
        Agregar Profesor
        -----------------
        """)
        print()
        nombres_pro = input("Nombre: ")
        apellido_pro = input("Apellido: ")
        catedra = input("Cátedra: ")
        print()
        Pro = Profesor(id=0, nombre=nombres_pro, apellido=apellido_pro, catedra=catedra)
        Profesor.objects.guardarProfesor(Pro)
        print("Profesor Ingresado con éxito")
        self.pausa()

    def agregarAlumnoCurso(self):
        print("""
        -----------------
        Asignar Alumno a Curso
        -----------------
        """)
        print()
        for alum in Alumno.objects.getAlumno(0):
            print(alum)

        alumno_id = int(input("Ingrese el número del Alumno "))
        alumno = Alumno.objects.getAlumnoId(alumno_id)
        self._listar_cursos()
        curso_id = int(input("Ingrese el número del curso "))
        alumno.curso = curso_id
        Alumno.objects.guardarAlumno(alumno)
        print("Alumno Ingresado a curso con éxito")
        self.pausa()

    def AsignarHorarioProfesorACurso(self):
        print("""
        -----------------
        Establecer Horario del Curso y asignar profesor 
        -----------------
        """)
        print()
        self._listar_cursos()
        curso_id = input("Ingrese número de Curso ")
        for pro in Profesor.objects.getProfesor():
            print(pro)
        profesor_id = input("Ingrese número de profesor ")
        print("""
        1. Lunes/ 2.Martes/ 3. Miércoles/ 4.Jueves
        5. Viernes/ 6. Sábado/ 7.Domingo    
        """)
        nro_dia = input("ingrese número de dia ")
        dia=self._dia_semana(int(nro_dia))
        print(dia)
        desde = input("Hora de inicio (HH:MM) ")
        hasta = input("Hora de fin (HH:MM) ")
        horario = Horario(id= 0, dia= dia, desde= desde, hasta= hasta, curso= curso_id, profesor= profesor_id)
        Horario.objects.guardarHorario(horario)
        print("Horario registrado con éxito")
        self.pausa()

    def verAlumnosPorCurso(self):
        print("""
        -----------------
        Ver Alumnos por Curso
        -----------------
        """)
        print()
        self._listar_cursos()
        curso_id = input("Ingrese número de Curso")
        for alum in Alumno.objects.getAlumno(curso_id):
            print(alum)
        self.pausa()
        
    def verHorarioProfesor(self):
        for pro in Profesor.objects.getProfesor():
            print(pro)
        profesor_id = int(input("Ingrese el número del profesor "))
        for horario in Horario.objects.horarioProfesor(profesor_id):
            print(horario)
        self.pausa()

    def verHorarioCurso(self):
        self._listar_cursos()
        curso_id = int(input("Ingrese curso a consultar "))
        for horario in Horario.objects.horarioCurso(curso_id):
            print(horario)
        self.pausa()

    def salir(self):
        print("Saliendo...")
        exit()

# funciones auxiliares  
    def _listar_cursos(self):
         for curs in Curso.objects.getCurso():
            print(curs)

    def _dia_semana(self, dia):
        switcher={
                1:'Lunes',
                2:'Martes',
                3:'Miércoles',
                4:'Jueves',
                5:'Viernes',
                6:'Sábado',
                7:'Domingo'
            }
        return switcher.get(dia,"Día Inválido")
        
    def pausa(self):
        print()
        input("Presione enter para continuar...")
  
obj = EscuelaManager()
obj.menu()